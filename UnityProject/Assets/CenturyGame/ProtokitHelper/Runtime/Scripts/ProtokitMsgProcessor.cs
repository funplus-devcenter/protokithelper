﻿using System;
using System.IO;
using gen.netutils;
using CenturyGame.Framework.Network;
using CenturyGame.Framework.Base;

namespace ProtokitHelper
{
    public class ProtokitMsgProcessor : Singleton<ProtokitMsgProcessor>, IMsgProcesser
    {
        private static readonly int Head_Size = 4;
        private readonly byte[] HeadBuff = new byte[Head_Size];

        public void WriteSendBytes(byte[] data, Stream stream)
        {
            stream.Position = 0;
            stream.SetLength(0);
            byte[] sendHeader = BitConverter.GetBytes(data.Length);
            stream.Write(sendHeader, 0, sendHeader.Length);
            stream.Write(data, 0, data.Length);
        }

        public bool ReadRecvBytes(Stream stream, out byte[] msgData)
        {
            msgData = new byte[0];
            if (!ReadMsgHead(stream, out int msgSize))
            {
                return false;
            }
            if (stream.Length - stream.Position < msgSize)
            {
                return false;
            }
            msgData = new byte[msgSize];
            stream.Read(msgData, 0, msgData.Length);
            return true;
        }

        private bool ReadMsgHead(Stream stream, out int msgSize)
        {
            msgSize = 0;
            if (stream.Length - stream.Position < Head_Size)
                return false;
            stream.Read(HeadBuff, 0, Head_Size);
            msgSize = BitConverter.ToInt32(HeadBuff, 0);
            return true;
        }

        public void DispatchMsg(byte[] data)
        {
            RawPacket rp;
            try
            {
                rp = RawPacket.Parser.ParseFrom(data);
            }
            catch (Exception e)
            {
                Trace.Instance.error("Exception throw :{0} on parsing data", e.Message);
                return;
            }
            for (int i = 0; i < rp.RawAny.Count; i++)
            {
                string name = rp.RawAny[i].Uri;
                byte[] rawdata = rp.RawAny[i].Raw.ToByteArray();
                string passthrough = rp.RawAny[i].PassThrough;
                ProtokitClient.Instance.RecvMsg(rp.SequenceID, passthrough, name, rawdata);
            }
            ProtokitClient.Instance.FinishRecvSequence(rp.SequenceID);
        }
    }
}