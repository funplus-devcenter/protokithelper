﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Google.Protobuf;
using gen.netutils;
using CenturyGame.Framework;
using CenturyGame.Framework.Base;
using CenturyGame.Framework.Network;

namespace ProtokitHelper
{
    public class ProtokitClient : Singleton<ProtokitClient>
    {
        private Queue<RawProto> SendQueue = new Queue<RawProto>();
        private Dictionary<string, Action<IMessage>> RequestHandlers = new Dictionary<string, Action<IMessage>>();
        private Dictionary<string, Action<IMessage>> MessageHandlers = new Dictionary<string, Action<IMessage>>();
        private Dictionary<int, RequestBatchRecord> RequestBatchRecordMap = new Dictionary<int, RequestBatchRecord>();

        /// <summary>
        /// 是否允许使用queue模式合并请求
        /// </summary>
        public bool EnableBatchRequest { get; private set; }

        /// <summary>
        /// 消息最大合并数
        /// </summary>
        public int MaxBatchCount { get; private set; } = 1;

        /// <summary>
        /// 消息接收
        /// </summary>
        public event DelegateRecvProto Evt_RecvMsg;
        /// <summary>
        /// 消息包解析完成
        /// </summary>
        public event DelegateRecvPacketFinish Evt_RecvPackFinish;
        /// <summary>
        /// Update轮询
        /// </summary>
        public event Action Evt_Update;

        private StringBuilder sb = new StringBuilder();

        public void Init(bool enableQueue, int batchLimit = 1)
        {
            EnableBatchRequest = enableQueue;
            MaxBatchCount = batchLimit;
        }

        public void SendMessage(IMessage message, Action<IMessage> handler = null)
        {
            var sendMsg = ProtokitUtil.Instance.GetRawPorto(message);
            SendQueue.Enqueue(sendMsg);
            if (handler != null)
                RequestHandlers.Add(sendMsg.Passthrough, handler);
        }

        /// <summary>
        /// 注册对某一类型消息的监听函数
        /// </summary>
        /// <param name="uri">消息名称</param>
        /// <param name="handler">消息处理回调</param>
        public void RegisterMessageHandler(string uri, Action<IMessage> handler)
        {
            if (MessageHandlers.ContainsKey(uri))
                MessageHandlers[uri] += handler;
            else
                MessageHandlers.Add(uri, handler);
        }

        /// <summary>
        /// 移除对某一类型消息的监听函数
        /// </summary>
        /// <param name="uri">消息名称</param>
        /// <param name="handler">消息处理回调</param>
        public void RemoveMessageHandler(string uri, Action<IMessage> handler)
        {
            if (MessageHandlers.ContainsKey(uri))
            {
                MessageHandlers[uri] -= handler;
                if (MessageHandlers[uri] == null)
                    MessageHandlers.Remove(uri);
            }
        }

        public void Update()
        {
            if (EnableBatchRequest)
            {
                if (SendQueue.Count > 0)
                {
                    RawPacket rp = new RawPacket
                    {
                        Version = 1,
                        SequenceID = ProtokitUtil.Instance.GetSequenceId()
                    };
                    RequestBatchRecord batchRecordItem = ProtokitUtil.Instance.GetRequestBatchRecord();
                    batchRecordItem.SequenceID = rp.SequenceID;
                    int batchCount = 0;
                    while (SendQueue.Count > 0 && batchCount < MaxBatchCount)
                    {
                        var msg = SendQueue.Dequeue();
                        RawAny rawAny = new RawAny
                        {
                            Uri = msg.MsgUri,
                            Raw = msg.MsgRaw,
                            PassThrough = msg.Passthrough
                        };
                        rp.RawAny.Add(rawAny);
                        batchRecordItem.Requests.Add(msg.Passthrough, msg.MsgUri);
                        batchCount++;
                        Trace.Instance.debug("[TCP][Send] {0}", msg.ProtoLog);
                    }
                    RequestBatchRecordMap.Add(batchRecordItem.SequenceID, batchRecordItem);
                    using (var stream = new MemoryStream())
                    {
                        rp.WriteTo(stream);
                        byte[] data = stream.ToArray();
                        GameLauncher.Network.SendMessage(data);
                    }
                }
            }
            else
            {
                while (SendQueue.Count > 0)
                {
                    RawPacket rp = new RawPacket
                    {
                        Version = 1,
                        SequenceID = ProtokitUtil.Instance.GetSequenceId()
                    };
                    RequestBatchRecord batchRecordItem = ProtokitUtil.Instance.GetRequestBatchRecord();
                    batchRecordItem.SequenceID = rp.SequenceID;
                    var msg = SendQueue.Dequeue();
                    RawAny rawAny = new RawAny
                    {
                        Uri = msg.MsgUri,
                        Raw = msg.MsgRaw,
                        PassThrough = msg.Passthrough
                    };
                    rp.RawAny.Add(rawAny);
                    batchRecordItem.Requests.Add(msg.Passthrough, msg.MsgUri);
                    using (var stream = new MemoryStream())
                    {
                        rp.WriteTo(stream);
                        byte[] data = stream.ToArray();
                        GameLauncher.Network.SendMessage(data);
                        Trace.Instance.debug("[TCP][Send] {0}", msg.ProtoLog);
                    }
                }
            }
            Evt_Update?.Invoke();
        }

        public void RecvMsg(int sequenceId, string passthrough, string uri, byte[] data)
        {
            try
            {
                sb.Length = 0;
                sb.AppendFormat("[TCP][Recv] name:{0}", uri);
                if (MessageHandlers.ContainsKey(uri))
                {
                    var parser = ProtokitUtil.Instance.GetParser(uri);
                    if (parser != null)
                    {
                        var message = parser.ParseFrom(data);
                        sb.AppendFormat(", body:{0}", message);
                        MessageHandlers[uri].Invoke(message);
                    }
                    else
                        Trace.Instance.warn($"can't find message parser, uri={uri}");
                }
                if (RequestHandlers.ContainsKey(passthrough))
                {
                    var parser = ProtokitUtil.Instance.GetParser(uri);
                    if (parser != null)
                    {
                        var message = parser.ParseFrom(data);
                        sb.AppendFormat(", body:{0}", message);
                        RequestHandlers[passthrough].Invoke(message);
                    }
                    else
                    {
                        Trace.Instance.warn($"can't find request parser, uri={uri}, sequenceId={sequenceId}, passthrough={passthrough}");
                    }
                    RequestHandlers.Remove(passthrough);
                }
                if (uri.Equals(ErrorResponse.Descriptor.FullName))
                {
                    ErrorResponse errmsg = ErrorResponse.Parser.ParseFrom(data);
                    sb.AppendFormat(", body:{0}", errmsg);
                    if (!errmsg.LogicException)
                    {
                        RecvCommonError(sequenceId, passthrough, errmsg);
                    }
                }
                Trace.Instance.debug("{0}", sb.ToString());
                Evt_RecvMsg?.Invoke(sequenceId, passthrough, uri, data);
            }
            catch (Exception e)
            {
                Trace.Instance.error($"ProtokiClient RecvMsg catch exception : {e.Message}, sequenceId={sequenceId}, passthrough={passthrough}, uri={uri}");
            }
        }

        /// <summary>
        /// 消息队列收取完成
        /// </summary>
        /// <param name="sequenceId"></param>
        public void FinishRecvSequence(int sequenceId)
        {
            ClearRequestHandler(sequenceId);
            Evt_RecvPackFinish?.Invoke(sequenceId);
        }

        /// <summary>
        /// 收到底层错误
        /// </summary>
        /// <param name="sequenceId"></param>
        /// <param name="passthrough"></param>
        /// <param name="msg"></param>
        private void RecvCommonError(int sequenceId, string passthrough, ErrorResponse msg)
        {
            string ErrReqUri = string.Empty;
            if (RequestBatchRecordMap.ContainsKey(sequenceId))
            {
                if (RequestBatchRecordMap[sequenceId].Requests.ContainsKey(passthrough))
                    ErrReqUri = RequestBatchRecordMap[sequenceId].Requests[passthrough];
            }
            ClearRequestHandler(sequenceId);
            Trace.Instance.warn($"ProtokitTcpClient receive common error. sequenceId={sequenceId}, passthrough={passthrough}, errorCode={msg.Code}, errorMsg={msg.Message}, errorReq={ErrReqUri}");
        }

        private void ClearRequestHandler(int sequenceId)
        {
            if (RequestBatchRecordMap.ContainsKey(sequenceId))
            {
                var batch = RequestBatchRecordMap[sequenceId];
                var e = batch.Requests.GetEnumerator();
                while (e.MoveNext())
                {
                    RequestHandlers.Remove(e.Current.Key);
                }
                RequestBatchRecordMap.Remove(sequenceId);
                ProtokitUtil.Instance.RecycleRequestBatch(batch);
            }
        }
    }
}